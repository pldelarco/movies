package com.sopra.movies.mapper;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.entity.MovieActor;

@Component
public class ActorMapper implements Mapper<Actor, ActorDTO> {

	
	public Actor toEntityFromDTO(ActorDTO actorDto) {
		Actor actor = new Actor();
		actor.setName(actorDto.getName());
		if(actorDto.getId() != 0) {
			actor.setId(actorDto.getId());
			actor.setMovies(actorDto.getMovies().stream()
					.map(movieDto -> toMovieActorFronMovieDTO(movieDto, actor))
					.collect(Collectors.toList()));
		}	
		return actor;
	}
	
	private MovieActor toMovieActorFronMovieDTO(MovieDTO movieDto,Actor actor) {
		Movie movie = new Movie();
		movie.setDirector(movieDto.getDirector());
		movie.setGenre(movieDto.getGenre());
		movie.setId(movieDto.getId());
		movie.setRate(movieDto.getRate());
		movie.setTitle(movieDto.getTitle());
		movie.setYear(movieDto.getYear());
		movie.setActors(new ArrayList<MovieActor>());
		return new MovieActor(movie,actor);
	}
	
	public ActorDTO toDTOFromEntity(Actor actor) {
		ActorDTO actorDto = new ActorDTO();
		actorDto.setId(actor.getId());
		actorDto.setName(actor.getName());
		actorDto.setMovies(actor.getMovies().stream()
				.map(movieActor -> toMovieDTOFromMovieActor(movieActor))
				.collect(Collectors.toList()));
	
		return actorDto;
	}
	
	private MovieDTO toMovieDTOFromMovieActor(MovieActor movieActor) {
		MovieDTO mdto = new MovieDTO();
		mdto.setDirector(movieActor.getMovie().getDirector());
		mdto.setGenre(movieActor.getMovie().getGenre());
		mdto.setId(movieActor.getMovie().getId());
		mdto.setRate(movieActor.getMovie().getRate());
		mdto.setTitle(movieActor.getMovie().getTitle());
		mdto.setYear(movieActor.getMovie().getYear());
		return mdto;
	}
}
