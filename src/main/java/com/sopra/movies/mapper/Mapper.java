package com.sopra.movies.mapper;

public interface Mapper <Entity, DTO>{
	public Entity toEntityFromDTO(DTO dto);
	public DTO toDTOFromEntity(Entity entity);
}
