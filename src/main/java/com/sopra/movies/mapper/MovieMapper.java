package com.sopra.movies.mapper;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.entity.MovieActor;

@Component
public class MovieMapper implements Mapper<Movie, MovieDTO> {
	@Autowired
	ActorMapper actorMapper;
	public Movie toEntityFromDTO(MovieDTO movieDto) {
		Movie movie = new Movie();
		movie.setDirector(movieDto.getDirector());
		movie.setGenre(movieDto.getGenre());
		movie.setRate(movieDto.getRate().doubleValue());
		movie.setTitle(movieDto.getTitle());
		movie.setYear(movieDto.getYear());
		if (movieDto.getId() != null) {
			movie.setId(movieDto.getId().intValue());
			movie.setActors(movieDto.getActors().stream()
					.map(actorDto -> toMovieActorFromActorDTO(movie, actorDto))
					.collect(Collectors.toList()));

		}
		return movie;
	}
	
	private MovieActor toMovieActorFromActorDTO(Movie movie,ActorDTO actorDto){
		Actor actor = new Actor();
		actor.setId(actorDto.getId());
		actor.setName(actorDto.getName());
		actor.setMovies(new ArrayList<MovieActor>());
		return new MovieActor(movie,actor);
	}

	public MovieDTO toDTOFromEntity(Movie movie) {
		MovieDTO movieDto = new MovieDTO();
		movieDto.setDirector(movie.getDirector());
		movieDto.setGenre(movie.getGenre());
		movieDto.setId(movie.getId());
		movieDto.setRate(movie.getRate());
		movieDto.setTitle(movie.getTitle());
		movieDto.setYear(movie.getYear());
		movieDto.setActors(movie.getActors().stream()
				.map(movieActor ->toActorDTOFromMovieActor(movieActor))
				.collect(Collectors.toList()) );
		
		return movieDto;
	}
	
	public MovieDTO toDTOFromEntity2Fields(Movie movie) {
		MovieDTO movieDto = new MovieDTO();
		movieDto.setTitle(movie.getTitle());
		movieDto.setYear(movie.getYear());
		return movieDto;
	}
	
	private ActorDTO toActorDTOFromMovieActor(MovieActor movieActor){
		ActorDTO adto = new ActorDTO();
		adto.setId(movieActor.getActor().getId());
		adto.setName(movieActor.getActor().getName());
		return adto;
	}
}
