package com.sopra.movies.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sopra.movies.exceptions.MoviesMovieNotFoundException;
import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.mapper.MovieMapper;
import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.services.MovieService;

@RestController
@RequestMapping("/movies")
public class MovieController {

	@Autowired
	private MovieService movieService;

	@Autowired
	private MovieMapper movieMapper;

	@Autowired
	private ActorMapper actorMapper;

	public MovieController(MovieService movieService, MovieMapper movieMapper, ActorMapper actorMapper) {
		this.movieService = movieService;
		this.actorMapper = actorMapper;
		this.movieMapper = movieMapper;
	}

	@GetMapping(path = "", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<MovieDTO>> movies() {
		List<Movie> movieList = movieService.getMovies();
		return new ResponseEntity<>(
				movieList.stream().map(movie -> movieMapper.toDTOFromEntity2Fields(movie)).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	@GetMapping(path = "{search}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<MovieDTO>> findMovie(@PathVariable("search") String search) {
		List<Movie> movieList = movieService.getMoviesByDirector(search);
		movieList.addAll(movieService.getMoviesByTitle(search));
		if (movieList.size() == 0) {
			movieList = movieService.getMovieByGenre(search);
		}
		return new ResponseEntity<>(movieList.stream().map(movie -> movieMapper.toDTOFromEntity(movie)).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	@PostMapping(path = "", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> createMovie(@Valid @RequestBody MovieDTO movieDto) {
		Movie movie = movieService.createMovie(movieDto);

		HttpHeaders header = new HttpHeaders();
		URI locationUri = ServletUriComponentsBuilder.fromCurrentRequest().path("/" + movie.getId())
				.build().toUri();
		header.setLocation(locationUri);

		return new ResponseEntity<>(header, HttpStatus.CREATED);
	}

	@PostMapping(path = "/{idMovie}/actors", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> addActorToMovie(@PathVariable("idMovie") Integer idMovie,
			@Valid @RequestBody ActorDTO actorDto) {
		Actor actor = actorMapper.toEntityFromDTO(actorDto);
		Movie movie = movieService.getMovieByid(idMovie).map(film -> movieService.addActorToMovie(film, actor))
				.orElseThrow(() -> new RuntimeException());

		HttpHeaders header = new HttpHeaders();
		URI locationUri = ServletUriComponentsBuilder.fromCurrentRequest().path("/" + actor.getId())
				.build().toUri();
		header.setLocation(locationUri);

		return new ResponseEntity<>(header, HttpStatus.ACCEPTED);

	}

	@PutMapping(path = "/{idMovie}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> updateMovie(@PathVariable("idMovie") Integer idMovie, @Valid @RequestBody MovieDTO movieDto) {
			movieService.updateMovie(movieDto,idMovie);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Void> deleteMovie(@PathVariable("id") int id) {
		movieService.deleteMovie(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

	
	

}
