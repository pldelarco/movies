package com.sopra.movies.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.services.ActorService;

@RestController
@RequestMapping("/actors")
public class ActorController {

	@Autowired
	private ActorService actorService;

	@Autowired
	private ActorMapper actorMapper;

	public ActorController(ActorService actorService, ActorMapper actorMapper) {
		this.actorService = actorService;
		this.actorMapper = actorMapper;
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<ActorDTO>> actors() {
		List<Actor> actorList = actorService.getActors();
		return new ResponseEntity<>(
				actorList.stream().map(actor -> actorMapper.toDTOFromEntity(actor)).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	@GetMapping(path = "/{search}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<ActorDTO>> findActor(@PathVariable("search") String search) {
		List<Actor> actorList = actorService.getActorByName(search);
		return new ResponseEntity<>(
				actorList.stream().map(actor -> actorMapper.toDTOFromEntity(actor)).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	@PostMapping(path = "/", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> createActor(@Valid @RequestBody ActorDTO actorDto) {
		Actor actor = actorService.createActor(actorDto);

		HttpHeaders header = new HttpHeaders();
		URI locationUri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/" + String.valueOf(actor.getId())).build().toUri();
		header.setLocation(locationUri);

		return new ResponseEntity<>(header, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> updateActor(@PathVariable("id") Integer id, @Valid @RequestBody ActorDTO actorDto) {
		if (id == actorDto.getId()) {
			actorService.updateActor(actorDto);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> deleteActor(@PathVariable("id") int id) {
		actorService.deleteActor(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
