package com.sopra.movies.exceptions;

public class MoviesActorNotFoundException extends MoviesException {

	public MoviesActorNotFoundException(Integer error, String msg) {
		super(error, msg);
	}

}
