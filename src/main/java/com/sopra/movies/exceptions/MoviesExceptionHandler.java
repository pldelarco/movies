package com.sopra.movies.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@RestControllerAdvice
public class MoviesExceptionHandler {

	@ExceptionHandler(value= {MoviesMovieNotFoundException.class})
	public ResponseEntity<Void>  movieNotFoundRequest(MoviesException ex){
		return ResponseEntity.notFound().build();
	}
	
	@ExceptionHandler(value= {MoviesActorNotFoundException.class})
	public ResponseEntity<Void>  actorNotFoundRequest(MoviesException ex){
		return ResponseEntity.notFound().build();
	}
	
}
