package com.sopra.movies.exceptions;

public class MoviesException extends RuntimeException{
	Integer error ;
	public MoviesException(Integer error, String msg) {
		super(msg);
		this.error=error;
	}
	public Integer getError() {
		return error;
	}
	public void setError(Integer error) {
		this.error = error;
	}
	
}
