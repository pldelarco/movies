package com.sopra.movies.exceptions;

public class MoviesMovieNotFoundException extends MoviesException {

	public MoviesMovieNotFoundException(Integer error, String msg) {
		super(error, msg);
	}

}
