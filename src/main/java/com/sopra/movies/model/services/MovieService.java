package com.sopra.movies.model.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sopra.movies.exceptions.MoviesMovieNotFoundException;
import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.mapper.MovieMapper;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.entity.MovieActor;
import com.sopra.movies.model.persistance.ActorDAO;
import com.sopra.movies.model.persistance.MovieDAO;

@Service
public class MovieService {

	private MovieDAO movieDAO;

	private ActorDAO actorDAO;
	
	private MovieMapper movieMapper;
	
	private ActorMapper actorMapper;
	@Autowired
	public MovieService(MovieDAO movieDAO, ActorDAO actorDAO, MovieMapper movieMapper, ActorMapper actorMapper) {
		super();
		this.movieDAO = movieDAO;
		this.actorDAO = actorDAO;
		this.movieMapper=movieMapper;
		this.actorMapper=actorMapper;
	}

	public List<Movie> getMovies() {
		return movieDAO.findAll();
	}

	public List<Movie> getMoviesByTitle(String title) {
		return movieDAO.findByTitleContainingIgnoreCase(title);
	}

	public Optional<Movie> getMovieByid(int id) {
		return movieDAO.findById(id);
	}

	public List<Movie> getMoviesByDirector(String search) {
		return movieDAO.findByDirectorContainingIgnoreCase(search);
	}

	public List<Movie> getMovieByGenre(String genre) {
		return movieDAO.findByGenreIgnoreCase(genre);
	}

	public Movie createMovie(MovieDTO movieDto) {
		Movie movie = movieMapper.toEntityFromDTO(movieDto);
		movieDAO.save(movie);
	    movieDto.getActors().stream().forEach(actorDto -> addActorToMovie(movie,actorMapper.toEntityFromDTO(actorDto)));
		return movie;
	}

	public Movie updateMovie(MovieDTO movieDto, Integer idMovie) {
		if (idMovie != movieDto.getId()) {throw new MoviesMovieNotFoundException(1, "Id not valid");}
		Movie movie = movieMapper.toEntityFromDTO(movieDto);
		return movieDAO.save(movie);
	}

	public Movie addActorToMovie(Movie movie, Actor actor) {
		if (!movie.getActors().stream().filter(a -> a.getActor().getId() == actor.getId()).findAny().isPresent()) {
			actorDAO.findById(actor.getId()).orElse(actorDAO.save(actor));
			MovieActor ma = new MovieActor();
			ma.setActor(actor);
			ma.setMovie(movie);
			movie.getActors().add(ma);
		}
		return movieDAO.save(movie);
	}

	public void deleteMovie(Integer id) {
		movieDAO.deleteById(id);
	}

	

}
