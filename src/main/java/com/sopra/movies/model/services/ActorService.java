package com.sopra.movies.model.services;

import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.persistance.ActorDAO;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorService {

	private ActorDAO actorDAO;
	
	private ActorMapper actorMapper;

	@Autowired
	public ActorService(ActorDAO actorDAO, ActorMapper actorMapper) {
		this.actorMapper=actorMapper;
		this.actorDAO = actorDAO;
	}

	public List<Actor> getActors() {
		return actorDAO.findAll();
	}

	public List<Actor> getActorByName(String name) {
		return actorDAO.findByNameContaining(name);
	}

	public Actor getActorbyId(int id) {
		return actorDAO.findById(id).orElseThrow(() -> new IllegalArgumentException());
	}

	public Actor updateActor(ActorDTO actorDto) {
		Actor actor = actorMapper.toEntityFromDTO(actorDto);
		return actorDAO.save(actor);
	}

	public Actor createActor(ActorDTO actorDto) {
		Actor actor = actorMapper.toEntityFromDTO(actorDto);
		return actorDAO.save(actor);
	}

	public void deleteActor(Integer id) {
		actorDAO.deleteById(id);
	}

	
}
