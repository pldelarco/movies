package com.sopra.movies.model.persistance;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sopra.movies.model.entity.Movie;;

@Repository
public interface MovieDAO extends JpaRepository<Movie,Integer>{
	
	public List<Movie> findByDirectorContainingIgnoreCase(String director);
	

	public List<Movie> findByTitleContainingIgnoreCase(String title);
	
	public List<Movie> findByGenreIgnoreCase(String genre);
}
