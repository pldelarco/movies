package com.sopra.movies.model.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sopra.movies.model.entity.Actor;

public interface ActorDAO extends JpaRepository<Actor, Integer> {
	public List<Actor> findByNameContaining(String name);
}
