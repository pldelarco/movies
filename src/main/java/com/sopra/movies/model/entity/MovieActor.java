package com.sopra.movies.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MovieActor implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn
	private Movie movie;
	
	@ManyToOne
	@JoinColumn
	private Actor actor;
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	public MovieActor() {
		this.movie=new Movie();
		this.actor=new Actor();
	}
	public MovieActor(Movie movie, Actor actor) {

		this.movie = movie;
		this.actor = actor;
	}

	
	

}
