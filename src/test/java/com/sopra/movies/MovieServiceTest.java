package com.sopra.movies;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.BDDMockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.mapper.MovieMapper;
import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.entity.MovieActor;
import com.sopra.movies.model.persistance.ActorDAO;
import com.sopra.movies.model.persistance.MovieDAO;
import com.sopra.movies.model.services.MovieService;

public class MovieServiceTest {

	private MovieService movieService;

	@Mock
	private MovieDAO movieDAO;
	@Mock
	private ActorDAO actorDAO;
	@Mock
	private ActorMapper actorMapper;
	@Mock
	private MovieMapper movieMapper;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
		movieService = new MovieService(movieDAO, actorDAO, movieMapper, actorMapper);
	}

	@Test
	public void shouldReturnAMovieListWithSizeN() {
		// given
		List<Movie> movieList = new ArrayList<Movie>();
		for (int i = 0; i < 5; i++) {
			movieList.add(new Movie());
		}
		when(movieDAO.findAll()).thenReturn(movieList);
		// when
		List<Movie> result = movieService.getMovies();
		// then
		assertEquals(5, result.size());

	}

	@Test
	public void shouldReturnAMovieListWithSizeNWhenContainTitle() {
		// given
		String title = "movie";
		List<Movie> movieList = new ArrayList<Movie>();
		for (int i = 0; i < 5; i++) {
			movieList.add(new Movie());
			movieList.get(i).setTitle(title + i);
		}
		when(movieDAO.findByTitleContainingIgnoreCase(title)).thenReturn(movieList);
		// when
		List<Movie> result = movieService.getMoviesByTitle(title);
		// then
		assertEquals(5, result.size());
		for (int i = 0; i < 5; ++i) {
			assertTrue(result.get(i).getTitle().contains(title));
		}
	}

	@Test
	public void shouldReturnAMovieListWithSizeNWhenContainDirector() {
		// given
		String director = "director";
		List<Movie> movieList = new ArrayList<Movie>();
		for (int i = 0; i < 5; i++) {
			movieList.add(new Movie());
			movieList.get(i).setDirector(director + i);
		}
		when(movieDAO.findByDirectorContainingIgnoreCase(director)).thenReturn(movieList);
		// when
		List<Movie> result = movieService.getMoviesByDirector(director);
		// then
		assertEquals(5, result.size());
		for (int i = 0; i < 5; ++i) {
			assertTrue(result.get(i).getDirector().contains(director));
		}
	}

	@Test
	public void shouldReturnAMovieListWithSizeNWhenGetGenre() {
		// given
		String genre = "genre";
		List<Movie> movieList = new ArrayList<Movie>();
		for (int i = 0; i < 5; i++) {
			movieList.add(new Movie());
			movieList.get(i).setGenre(genre);
		}
		when(movieDAO.findByGenreIgnoreCase(genre)).thenReturn(movieList);
		// when
		List<Movie> result = movieService.getMovieByGenre(genre);
		// then
		assertEquals(5, result.size());
		for (int i = 0; i < 5; ++i) {
			assertTrue(result.get(i).getGenre().equals(genre));
		}
	}

	@Test
	public void shouldReturnAnOptionalOfMovieNotEmptyWhenIdExist() {
		// given
		Movie movie = new Movie();
		movie.setId(1);
		movie.setDirector("director");
		movie.setGenre("genre");
		movie.setRate(10.0);
		movie.setYear("2019");
		movie.setActors(new ArrayList<MovieActor>());
		movie.setTitle("title");
		when(movieDAO.findById(1)).thenReturn(Optional.of(movie));
		// when
		Optional<Movie> result = movieService.getMovieByid(1);
		// then
		assertTrue(result.isPresent());
	}

	@Test
	public void shouldReturnAnOptionalOfMovieEmptyWhenIdNotExist() {
		// given

		when(movieDAO.findById(1)).thenReturn(Optional.empty());
		// when
		Optional<Movie> result = movieService.getMovieByid(1);
		// then
		assertFalse(result.isPresent());
	}

	@Test
	public void shouldReturnAMovieWithNewID() {
		// given
		MovieDTO movieDto = new MovieDTO();
		movieDto.setActors(new ArrayList<>());
		Movie movie = new Movie();
		Actor actor = new Actor();
		ActorDTO actorDto = new ActorDTO();
		movie.setId(1);
		movie.setDirector("director");
		movie.setGenre("genre");
		movie.setRate(10.0);
		movie.setYear("2019");
		movie.setActors(new ArrayList<MovieActor>());
		movie.setTitle("title");
		when(movieMapper.toEntityFromDTO(movieDto)).thenReturn(movie);
		when(movieDAO.save(movie)).thenReturn(movie);
		when(actorMapper.toEntityFromDTO(actorDto)).thenReturn(actor);
		when(movieService.addActorToMovie(movie,actor)).thenReturn(movie);
		// when
		Movie result = movieService.createMovie(movieDto);
		// then
		assertEquals(1, result.getId());

	}

	@Test
	public void shouldReturnAMovieWithModifiedFields() {
		// given
		Integer idMovie=1;
		MovieDTO movieDto = new MovieDTO();
		Movie movie = new Movie();
		movie.setId(idMovie);
		movie.setDirector("director");
		movie.setGenre("genre");
		movie.setRate(10.0);
		movie.setYear("2019");
		movie.setActors(new ArrayList<MovieActor>());
		movie.setTitle("title");
		when(movieDAO.save(movie)).thenReturn(movie);
		when(movieMapper.toEntityFromDTO(movieDto)).thenReturn(movie);
		// when
		Movie result = movieService.updateMovie(movieDto,idMovie);
		// then
		assertEquals(1, result.getId());
	}
}