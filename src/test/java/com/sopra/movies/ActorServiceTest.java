package com.sopra.movies;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.BDDMockito.when;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.sopra.movies.mapper.ActorMapper;
import com.sopra.movies.model.DTO.ActorDTO;
import com.sopra.movies.model.DTO.MovieDTO;
import com.sopra.movies.model.entity.Actor;
import com.sopra.movies.model.entity.Movie;
import com.sopra.movies.model.entity.MovieActor;
import com.sopra.movies.model.persistance.ActorDAO;
import com.sopra.movies.model.services.ActorService;

public class ActorServiceTest {

	private ActorService actorService;

	@Mock
	private ActorDAO actorDAO;
	
	@Mock
	private ActorMapper actorMapper;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
		actorService = new ActorService(actorDAO,actorMapper);
	}

	@Test
	public void shouldReturnAListOfActorWithSizeN() {
		// given
		int tamLista = 5;
		List<Actor> actorList = new ArrayList<>();
		for (int i = 0; i < tamLista; ++i) {
			actorList.add(new Actor());
		}
		when(actorDAO.findAll()).thenReturn(actorList);
		// when
		List<Actor> result = actorService.getActors();
		// then
		assertEquals(tamLista, result.size());
	
	}

	@Test
	public void shouldReturnAlistOfActorWithSizeNWhenContainActorWithName() {
		int tamLista = 5;
		String name= "Juan";
		List<Actor> actorList = new ArrayList<>();
		for (int i = 0; i < tamLista; ++i) {
			actorList.add(new Actor());
			actorList.get(i).setName(name+i);
		}
		when(actorDAO.findByNameContaining(name)).thenReturn(actorList);
		// when
		List<Actor> result = actorService.getActorByName(name);
		// then
		assertEquals(tamLista, result.size());
		for (int i = 0; i < result.size(); ++i) {
			assertTrue(result.get(i).getName().contains(name));
		}
	}
	
	@Test
	public void shouldReturnAlistOfActorWithSize0WhenNotContainActorWithName() {
	
		String name= "Juan";
		List<Actor> actorList = new ArrayList<>();
		when(actorDAO.findByNameContaining(name)).thenReturn(actorList);
		// when
		List<Actor> result = actorService.getActorByName(name);
		// then
		assertEquals(0, result.size());
	}
	
	@Test
	public void shouldReturnAOptionalOfActorWhenIDIsInDB() {
	
		Integer id = 1;
		Actor actor= new Actor();
		actor.setId(id);
		Optional<Actor> oActor = Optional.of(actor);
		when(actorDAO.findById(id)).thenReturn(oActor);
		// when
		Actor result = actorService.getActorbyId(id);
		// then
		assertEquals(id, result.getId());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldReturnAnIllegalArgumentException() {
		int id=0;
		when(actorDAO.findById(id)).thenThrow(IllegalArgumentException.class);
		// when
		Actor result = actorService.getActorbyId(id);
	}
	
	@Test
	public void shouldReturnAnActorWithModificatedFields() {
		//given

		ActorDTO actorDto = new ActorDTO();
		Actor actor= new Actor();
		actor.setId(1);
		actor.setName("Juan");
		when(actorDAO.save(actor)).thenReturn(actor);
		when(actorMapper.toEntityFromDTO(actorDto)).thenReturn(actor);
		//when
		Actor result = actorService.updateActor(actorDto);
		//then
		assertEquals(actor.getId(),result.getId());
		assertEquals(actor.getName(),result.getName());
		assertEquals(actor.getMovies(),result.getMovies());
		
	}
	
	@Test
	public void shouldReturnAnActorWithNewID() {
		//given
		ActorDTO actorDto = new ActorDTO();
		Actor actor= new Actor();
		actor.setId(1);
		actor.setName("Juan");
		when(actorDAO.save(actor)).thenReturn(actor);
		when(actorMapper.toEntityFromDTO(actorDto)).thenReturn(actor);
		//when
		Actor result = actorService.createActor(actorDto);
		//then
		assertEquals(actor.getId(),result.getId());
		assertEquals(actor.getName(),result.getName());
		assertEquals(actor.getMovies(),result.getMovies());
		
	}
	
	

}
